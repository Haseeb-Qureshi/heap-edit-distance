
Herbert Heapenson is writing a web tracker for Heap. He's built an initial prototype that captures DOM hierarchies for click events and sends them to the Heap backend. That is, each time a click happens, his tracker captures the hierarchy of the target DOM element. (See this link for more concrete examples of DOM hierarchies.)

Each hierarchy is a space-delimited, ordered list of DOM elements, represented as strings with the following components:

tagname
'#' + id
'.' + class
Each element in the hierarchy has the following properties:

The tagname must exist.
There can be at most one id.
Any number of classes is valid.
An element is guaranteed to start with the tagname, followed by the id, if it exists, followed by the classes.
There will be no repeated classes.
Any tagname/id/class is guaranteed to contain only underscores, dashes, alphanumeric characters (0-9, a-z, A-Z).
Some example hierarchies sent by his tracker are:

'div.main li.feature.list a#retroactive'
'section div#team-page.banner img.mat'
'div.footer.fixed a#signup.blue.btn'
'div.footer.fixed a#login.blue.btn'
Herbert notices that many of these hierarchies share similar paths. For instance, the last two examples above are almost identical.

This insight leads Herbert to believe that he can write an encoding algorithm to compress these hierarchies, saving Heap lots of bandwidth and storage costs. His first step is to find an efficient algorithm for computing the distance between two DOM hierarchies.

Given two hierarchies h1 and h2, the distance is defined as the smallest number of transformations needed to get from h1 to h2. (Note: this is not the same as the distance between h2 and h1, as order matters.) A transformation involves making one of the following changes to the hierarchy:

Changing the tag of any element in the hierarchy.
Inserting a new tag anywhere into the hierarchy.
Removing a tag from the hierarchy (along with all of its classes and its id, if relevant).
Adding an id to a tag, if one doesn’t already exist.
Removing the id of a tag.
Adding a class to a tag.
Removing a class from a tag.
For example, the minimum distance between these two hierarchies:

'div.footer.fixed a#signup.blue.btn'
'div.header li.btn a#signup'
is 7.
Add new tag li in the middle.
Add class .btn to li.
Remove .blue from a.
Remove .btn from a.
Remove .footer from div.
Remove .fixed from div.
Add .header to div.
As another example, the minimum distance between:

'div.green.dotted a#login'
'a#login div.green.dotted'
is 3.
Remove a#login tag.
Add a tag to beginning.
Add #login to a.
Feel free to use any utility libraries you think will be helpful. Please keep your code clean and organized, with comments if the logic is particularly complex. When evaluating your code, this will be just as important to us as functionality and speed.

There are 33 test cases provided here. Each test case is in the form:

foo.bar
baz.quux
n
where foo.bar and baz.quux are hierarchies, and n is the distance between them. Your solution should be able to process all 32 test cases in less than a second.
