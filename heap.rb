require 'colorize'

class HeapDataComparer
  SYMBOL_SIZE = 10 ** 5
  def initialize
    # i transform each tag, id, and class into symbols for faster manipulation
    # i.e., div#root.large.huge.jumbo would become =>
    # [1, 10000, 20000, 200001, 200002, 200003]

    # symbol size must be sufficiently large to span the alphabet of
    # classes or ids.

    # i then initialize maps to retain the alphabet mapping.
    @tagnames_mapping, @next_tagname_symbol = {}, 0
    @ids_mapping, @next_id_symbol = {}, SYMBOL_SIZE
    @classes_mapping, @next_class_symbol = {}, SYMBOL_SIZE * 2
  end

  def edit_distance(str1, str2)
    seq1 = transform_hierarchy(str1).flatten
    seq2 = transform_hierarchy(str2).flatten

    # initialize dp array
    dp_arr = dp_arr = Array.new(seq1.length + 1) { Array.new(seq2.length + 1) { 0 } }
    dp_arr.first.each_with_index { |el, i| dp_arr.first[i] = i }

    # a little tricky here to account for atomic tag deletion
    (1...dp_arr.length).inject(0) do |num_tags, i|
      num_tags += 1 if is_tag?(seq1[i - 1])
      dp_arr[i][0] = num_tags
    end

    # iterate through dp array
    previous_tag = 1
    (1...dp_arr.length).each do |i|
      # save where the last tag began, so i can look up tag deletion index in O(1) time
      previous_tag = i if is_tag?(seq1[i - 1])
      (1...dp_arr[0].length).each do |j|
        addition = dp_arr[i][j - 1] + 1
        deletion = dp_arr[i - 1][j] + 1

        substitution = Float::INFINITY
        # can only do substitutions if the two elements are identical (no cost)
        # or they're both tags, i can replace the tag name in one operation
        if seq1[i - 1] == seq2[j - 1]
          substitution = dp_arr[i - 1][j - 1]
        elsif is_tag?(seq1[i - 1]) && is_tag?(seq2[j - 1])
          substitution = dp_arr[i - 1][j - 1] + 1
        end

        # i can also delete an entire tag by looking back to where a tag began
        delete_entire_tag = dp_arr[previous_tag - 1][j] + 1

        # take the lowest cost path of every option
        dp_arr[i][j] = [addition, deletion, substitution, delete_entire_tag].min
      end
    end

    dp_arr.last.last
  end

  def transform_hierarchy(str)
    selectors = str.split # autosplits on spaces

    selectors.map do |selector|
      match_data = selector.match(/^([^#|.]+)(.*)/)
      tagname = match_data[1]
      non_tag_stuff = match_data[2]
      transform_to_metasymbols!(tagname, non_tag_stuff) # do crazy mapping stuff
    end
  end

  def run_tests(filename, just_print_total = false)
    lines = File.readlines(filename).map(&:chomp)
    total_tests = lines.size / 3
    tests_passed = 0
    i = 1

    lines.each_slice(3) do |str1, str2, expected_output|
      if !just_print_total
        puts "-----" * 10
        puts "TEST #{i}"
        puts str1
        puts str2
      end

      output = edit_distance(str1, str2)
      if output == expected_output.to_i
        puts "Test passed!".green if !just_print_total
        tests_passed += 1
      elsif !just_print_total
        puts "For #{str1} vs #{str2}:\n" + "You got: #{output}. Expected: #{expected_output}.".red
      end
      i += 1
    end

    puts "Passed a total of " + "#{tests_passed} / #{total_tests}".bold
  end

  private

  def is_tag?(val)
    val < SYMBOL_SIZE
  end

  def transform_to_metasymbols!(tagname, non_tag_stuff)
    # crazy mapping stuff
    # very ugly, but it unfortunately works. would really want to refactor this.
    unless @tagnames_mapping[tagname]
      @tagnames_mapping[tagname] = @next_tagname_symbol
      @next_tagname_symbol += 1
    end

    this_tag_symbol = @tagnames_mapping[tagname]
    this_id_symbol = nil
    these_class_symbols = []

    unless non_tag_stuff.empty?
      id = nil
      classes = []

      case
      when non_tag_stuff.match(/^(#[^\.]+)(\..+)/)
        data = non_tag_stuff.match(/^(#[^\.]+)(\..+)/)
        id = data[1]
        classes = data[2].split(".").reject(&:empty?)
      when non_tag_stuff.match(/^\..+/)
        classes = non_tag_stuff.split(".").reject(&:empty?)
      when non_tag_stuff.match(/^#.+/)
        id = non_tag_stuff
      end

      if id && !@ids_mapping.has_key?(id)
        @ids_mapping[id] = @next_id_symbol
        @next_id_symbol += 1
      end
      this_id_symbol = @ids_mapping[id]

      these_class_symbols = classes.map do |classname|
        unless @classes_mapping[classname]
          @classes_mapping[classname] = @next_class_symbol
          @next_class_symbol += 1
        end
        @classes_mapping[classname]
      end
    end

    [this_tag_symbol, this_id_symbol, these_class_symbols.sort].flatten.compact
  end
end

hdc = HeapDataComparer.new

hdc.run_tests("failing_tests.txt")

hdc.run_tests("tests.txt", false)
